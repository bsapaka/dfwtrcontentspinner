<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Generate City Reports</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="styles.css">
</head>
<body>
<div class="container">
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div id="navbar">
				<ul class="nav navbar-nav">
					<li><a href="index.php">Home</a></li>
					<li class="active"><a href="about.php">About</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="jumbotron">
		<p>This application is an advanced article spinner specialized for MLS real estate data.</p>
		<hr/>
		<p>Try the app:</p>
		<ol>
			<li><a href="demoReports.zip">Download this sample .zip.</a></li>
			<li><a href="index.php">Upload it on the homepage and click "Create Articles".</a></li>
		</ol>
	</div>

</div>
</body>
</html>
