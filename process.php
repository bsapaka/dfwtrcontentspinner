<?php
require "vendor/autoload.php";
use App\Service;

Service::make($_FILES['zipfile'])->generate();

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Generate City Reports</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="styles.css">
</head>
<body>
<div class="container">
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div id="navbar">
				<ul class="nav navbar-nav">
					<li class="active"><a href="index.php">Home</a></li>
					<li><a href="about.php">About</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="jumbotron">
		<p>Processing...</p>
	</div>

</div>
</body>
</html>
