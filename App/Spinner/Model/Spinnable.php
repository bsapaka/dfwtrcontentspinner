<?php namespace App\Spinner\Model;


interface Spinnable {

	public function spin();

}