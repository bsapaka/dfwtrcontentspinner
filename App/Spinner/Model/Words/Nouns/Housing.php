<?php namespace App\Spinner\Model\Words\Nouns;

use App\Spinner\Model\Words\Noun;

class Housing extends Noun {

	protected $synonyms = array(
		'home',
		'home',
		'housing',
		'housing',
		'real estate',
		'real estate',
		'real estate',
		'real estate',
		'property',
		'property',
		'residential housing',
		'residential property',
	);
}