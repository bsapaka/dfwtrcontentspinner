<?php namespace App\Spinner\Model\Words\Nouns;

use App\Spinner\Model\Words\Noun;

class Count extends Noun {

	protected $synonyms = array(
		'the count',
		'the number'
	);

}