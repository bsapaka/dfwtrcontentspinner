<?php namespace App\Spinner\Model\Words\Nouns;

use App\Spinner\Model\Words\Noun;

class House extends Noun {

	protected $synonyms = array(
		'house',
		'house',
		'house',
		'house',
		'home',
		'home',
		'home',
		'home',
		'property',
		'property',
		'residential property',
		'unit',
		'housing unit',
	);
}