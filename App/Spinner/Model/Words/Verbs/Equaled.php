<?php namespace App\Spinner\Model\Words\Verbs;

use App\Spinner\Model\Words\Verb;

class Equaled extends Verb {

	protected $synonyms = array(
		'was'
	);

}