<?php

function dd($a) {
	echo "<pre>";
	print_r($a);
	echo "</pre>";
	exit;
}

/**
 * @param array $array
 * @param string $prefix
 * @return array
 */
function arrayPrefix(array $array, $prefix) {
	array_walk($array, function(&$str) use ($prefix) {
		$str = $prefix . " " . $str;
	});
	return $array;
}

/**
 * @param array $array
 * @param string $postfix
 * @return array
 */
function arrayPostfix(array $array, $postfix) {
	array_walk($array, function(&$str) use ($postfix) {
		$str = $str . " " . $postfix;
	});
	return $array;
}

/**
 * @param array $array
 * @param string $prefix
 * @param string $postfix
 * @return array
 */
function arrayFixes(array $array, $prefix = null, $postfix = null) {
	array_walk($array, function(&$str) use ($prefix, $postfix) {
		$str = $prefix . " " . $str . " " . $postfix;
	});
	return $array;
}

/**
 * @param $array
 * @return string
 */
function concatenate($array) {
	$concatenated = "";
	foreach($array as $elem) {
		$concatenated .= $elem . " ";
	}
	return $concatenated;
}