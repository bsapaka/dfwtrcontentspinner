<?php


use App\Spinner\Inflect;
use App\Spinner\Spinner;
use App\Scraper\PdfScraper;

use App\Spinner\Model\Phrases\PercentDifference;
use App\Spinner\Model\Phrases\PercentIncrease;
use App\Spinner\Model\Phrases\PercentDecrease;
use App\Spinner\Model\Phrases\PercentHold;
use App\Spinner\Model\Sentences\SalesReported;
use App\Spinner\Model\NounPhrases\NewListings;

//$inflect = new Inflect();
//dd($inflect->pluralize('housing unit'));



$a = new NewListings();
dd($a->spin());

$spinner = new Spinner();
$a = $spinner->spinArray(array(
	'{foo|car<%bat%>}',
	'bar'
));

dd($a);


$scraper = new PdfScraper('pdf/addison.pdf');
$city = $scraper->extract();



function singular($word) {
	return Inflect::singularize($word);
}

function plural($word) {
	return Inflect::pluralize($word);
}




//dd($spinner->spinArray($held));


$monthName = $city->getMonthReport()->getCurrentYear()->month;
$year = $city->getMonthReport()->getCurrentYear()->year;

$housesSold = $city->getMonthReport()->getCurrentYear()->salesReported;
$change = $city->getMonthReport()->getChange()->salesReported;


$str = "In $monthName there were $housesSold houses sold, which was a $change% change from last year";
dd($str);
//
//$house = '{house|home|unit|property}';
//$cost = '{cost|price}';
//$costed = '{cost|was priced at|sold for}';
//
//$houseCost = [
//	"On average, a $house $costed 500",
//	"A $house $costed 500 on average",
//	"The average $cost for a $house was 500"
//];
//
//echo $spinner->spinArray($houseCost);
