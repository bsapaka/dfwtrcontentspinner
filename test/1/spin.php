<?php
require "vendor/autoload.php";

use App\Spinner\Inflect;
use App\Spinner\Spinner;
use App\Scraper\PdfScraper;
use App\Models\City;


use App\Spinner\Model\Passages\SalesReported;
use App\Spinner\Model\Passages\SalesProjected;
use App\Spinner\Model\Passages\NewListings;
use App\Spinner\Model\Passages\ContractListings;
use App\Spinner\Model\Passages\MonthsSupply;
use App\Spinner\Model\Passages\Inventory;
use App\Spinner\Model\Passages\PercentReceived;
use App\Spinner\Model\Passages\DaysOnMarket;

use App\Spinner\Model\Passages\AveragePrice;

use App\Cleaner\Cleaner;

$scraper = new PdfScraper('pdf/addison.pdf');
$spinner = new Spinner();
$city = $scraper->extract();




function makeArticleBody($city) {
	$passage = array(
		SalesReported::make()->setCity($city)->setCompare()->fullPassage(),
		SalesProjected::make()->setCity($city)->fullPassage(),
		NewListings::make()->setCity($city)->fullPassage(),
		ContractListings::make()->setCity($city)->setCompare()->fullPassage(),
		MonthsSupply::make()->setCity($city)->monthPassage(),
		Inventory::make()->setCity($city)->monthPassage(),
		DaysOnMarket::make()->setCity($city)->setCompare()->monthPassage(),
		PercentReceived::make()->setCity($city)->fullPassage()
	);
	return concatenate($passage);
}

dd($city->fullTableOutput());

$p1 = makeArticleBody($city);
$p2 = makeArticleBody($city);

$p1 = Cleaner::make($p1)->clean();


echo "<p>";
print_r($p1);
echo "</p>";
//echo "<hr>";
//echo "<p>";
//print_r($p2);
//echo "</p>";

//dd(concatenate($passage));

//dd($passage->monthPassage());
?>
